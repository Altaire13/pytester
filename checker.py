#!/usr/bin/python3

import sys
import subprocess
import os


def check_test(job_name, suffix):
    """
    Compare output from
        ./[job_name] < [job_name][suffix].test
    with file
        [job_name][suffix].ans
    """

    test_name = job_name + suffix
    if not os.path.isfile(test_name + '.test'):
        # print('No test file:', test_name + '.test')
        return

    with open(test_name + '.test') as test_file:
        out = subprocess.check_output(['./' + job_name], stdin=test_file)

    res = out.strip().decode('utf-8')

    with open(test_name + '.ans') as ans_file:
        right = ans_file.read().strip()

    # res = res.split('\n')[0]
    # right = right.split('\n')[0]

    res = res.split()
    right = right.split()

    is_ok = (res == right)

    # if not is_ok:
    # print(test_name, 'OK' if is_ok else 'Fail', len(out.split()))

    # if not is_ok:
        # print('    Expected:', right)
        # print('    Got:     ', res)

    return is_ok

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage:', sys.argv[0], 'job_name', 'suffix')
        print(check_test.__doc__)
        sys.exit(0)

    check_test(sys.argv[1], sys.argv[2])
