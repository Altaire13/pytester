#!/usr/bin/python3
'''Generates tests and checks solution

Generates test set of [n] tests with:
    ./[job_name]_gen.py > [job_name][middle][i].test
Generates answers with:
    ./[job_name].py > [job_name][middle][i].ans
Checks solution with:
    ./checker [job_name] [middle][i]
which in turn runs
    ./[job_name] < [job_name][middle][i].test

Mode switch:
    [n] -- generate and test
    [+n] -- generate only
    [-n] -- test only

    [0] -- not implemented
'''
import sys
import subprocess

import checker
import tester

if len(sys.argv) < 2:
    print('Usage:', sys.argv[0], 'job_name', 'middle', 'n[=10]', 'repeat[=1]')
    print(__doc__)
    sys.exit(0)

job_name = sys.argv[1]

if len(sys.argv) > 2:
    middle = sys.argv[2]
else:
    middle = '_test'

num_prefix = ''

if len(sys.argv) > 3:
    if sys.argv[3][0] == '+':
        gen, test = True, False
    elif sys.argv[3][0] == '-':
        gen, test = False, True
    elif sys.argv[3][0] == '_':
        gen, test = False, True
        num_prefix = '_'
        sys.argv[3] = sys.argv[3][1:]
    else:
        gen, test = True, True

    n = abs(int(sys.argv[3]))
else:
    gen, test = True, True
    n = 10

if len(sys.argv) > 4:
    repeat = int(sys.argv[4])
else:
    repeat = 1

for r in range(repeat):
    if gen:
        for i in range(n):
            test_name = job_name + middle + str(i)
            with open(test_name + '.test', 'w') as test_file:
                subprocess.call('./' + job_name + '_gen.py', stdout=test_file)
            with open(test_name + '.test') as test_file:
                with open(test_name + '.ans', 'w') as ans_file:
                    subprocess.call('./' + job_name + '.py', stdin=test_file,
                        stdout=ans_file)
            print(',', end='')
            sys.stdout.flush()
            # print(test_name, 'generated')
        # print('done generating')
        print()
    if test:
        res = 0
        for i in range(n):
            cur = checker.check_test(job_name, middle + num_prefix + str(i))
            if cur is not None:
                res += cur
                if cur:
                    print('.', end='')
                else:
                    print('!', end='')
            else:
                print('?', end='')
            sys.stdout.flush()
        print('Passed', res, '/', n)
        if res != n:
            sys.exit(-1)

#tester.test_all(job_name)


