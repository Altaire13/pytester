#!/usr/bin/python3

import sys
import subprocess

import checker


def test_all(job_name, middle='_test', n=10):
    '''
    Runs checker for [job_name] with [n] present tests:
        ./checker [job_name] [middle][i]
    '''
    for i in range(n):
        checker.check_test(job_name, middle + str(i))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage:', sys.argv[0], 'job_name', '[middle=_test]', '[n=10]')
        print(test_all.__doc__)
        sys.exit(0)

    job_name = sys.argv[1]

    if len(sys.argv) > 2:
        middle = sys.argv[2]
    else:
        middle = '_test'

    if len(sys.argv) > 3:
        n = int(sys.argv[3])
    else:
        n = 10

    test_all(job_name, middle, n)
